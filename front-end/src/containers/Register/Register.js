import React, {Component, Fragment} from 'react';

import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import {registerUser} from "../../store/actions/usersActions";

class Register extends Component {
    state = {
        username: '',
        password: '',
        displayName: '',
        phone: ''
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    submitFormHandler = event => {
        event.preventDefault();

        this.props.registerUser({...this.state});
    };

    render() {
        return (
            <Fragment>
                <h2>Register</h2>
                <Form onSubmit={this.submitFormHandler}>
                    <FormGroup row>
                        <Label for="username" sm={2}>Username</Label>
                        <Col sm={10}>
                            <Input
                                type="text" required
                                id="username" name="username"
                                value={this.state.username}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Label for="password" sm={2}>Password</Label>
                        <Col sm={10}>
                            <Input
                                type="password" required
                                id="password" name="password"
                                value={this.state.password}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Label for="nickname" sm={2}>Nickname</Label>
                        <Col sm={10}>
                            <Input
                                type="nickname" required
                                id="nickname" name="nickname"
                                value={this.state.nickname}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Label for="phone" sm={2}>Phone number</Label>
                        <Col sm={10}>
                            <Input
                                type="phone" required
                                id="phone" name="phone"
                                value={this.state.phone}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>

                    <FormGroup>
                        <Col sm={{offset: 2, size: 10}}>
                            <Button type="submit" color="primary">
                                Register
                            </Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    error: state.users.registerError
});

const mapDispatchToProps = dispatch => ({
    registerUser: userData => dispatch(registerUser(userData))
});

export default Register;