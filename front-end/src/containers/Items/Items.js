import React, {Component, Fragment} from 'react';
import {fetchItems} from "../../store/actions/itemsActions";
import {connect} from "react-redux";
import ItemsList from "../../components/ItemsList/ItemsList";

class Items extends Component {
    componentDidMount() {
        this.props.onFetchItems();
    }

    render() {
        return (
            <Fragment>
                <h2>All items</h2>

                {this.props.items.map(item => (
                    <ItemsList
                        key={item._id}
                        _id={item._id}
                        title={item.title}
                        price={item.price}
                        image={item.image}
                    />
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    items: state.items.items
});

const mapDispatchToProps = dispatch => ({
    onFetchItems: () => dispatch(fetchItems())
});

export default connect(mapStateToProps, mapDispatchToProps)(Items);