import React, {Component, Fragment} from 'react';

import {connect} from "react-redux";
import ItemForm from "../../components/ItemForm/ItemForm";
import {createItem} from "../../store/actions/itemsActions";
import {fetchCategories} from "../../store/actions/categoriesActions";

class NewItem extends Component {

    componentDidMount() {
        this.props.fetchCategories();
    }

    createItem = itemData => {
        this.props.onItemCreated(itemData).then(() => {
           this.props.history.push('/');
        });
    };
    render() {
        return (
            <Fragment>
                <h2>Create new item</h2>
                <ItemForm
                    onSubmit={this.createItem}
                    categories={this.props.categories}
                />
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    categories: state.categories.categories
})

const mapDispatchToProps = dispatch => ({
    onItemCreated: itemData => dispatch(createItem(itemData)),
    fetchCategories: () => dispatch(fetchCategories())
});

export default connect(mapStateToProps, mapDispatchToProps)(NewItem);