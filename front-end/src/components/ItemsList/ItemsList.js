import React from 'react';
import PropTypes from 'prop-types';
import {Card, CardBody} from "reactstrap";
import ItemThumbnail from "../ItemThumbnail/ItemThumbnail";
import {Link} from "react-router-dom";

const ItemsList = props => {
    return (
        <Card key={props._id} style={{marginBottom: '10px'}}>
            <CardBody>
                <ItemThumbnail image={props.image} />
                <Link to={'/products/' + props._id}>
                    {props.title}
                </Link>
                <strong style={{marginLeft: '10px'}}>
                    {props.price} KGS
                </strong>
            </CardBody>
        </Card>
    );
};

ItemsList.propTypes = {
    image: PropTypes.string,
    _id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
};

export default ItemsList;