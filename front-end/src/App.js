import React, {Component, Fragment} from 'react';
import {Container} from "reactstrap";
import {Route, Switch} from "react-router-dom";
import Toolbar from "./components/UI/Toolbar";
import Items from "./containers/Items/Items";
import Register from "./containers/Register/Register";


class App extends Component {
  render() {
    return (
        <Fragment>
          <header>
            <Toolbar/>
          </header>
          <Container style={{marginTop: '20px'}}>
            <Switch>
              <Route path="/" exact component={Items}/>
              <Route path="/register" exact component={Register}/>
              {/*<Route path="/login" exact component={Login}/>*/}
              {/*<Route path="/item/new" exact component={NewItem}/>*/}

            </Switch>
          </Container>
        </Fragment>
    );
  }
}

export default App;