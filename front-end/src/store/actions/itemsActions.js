import axios from '../../axiosapi';

export const FETCH_ITEMS_SUCCESS = 'FETCH_ITEMS_SUCCESS';
export const CREATE_ITEMS_SUCCESS = 'CREATE_ITEMS_SUCCESS';

export const fetchItemsSuccess = items => ({type: FETCH_ITEMS_SUCCESS, items});
export const createItemsSuccess = () => ({type: CREATE_ITEMS_SUCCESS});

export const fetchItems = () => {
    return dispatch => {
        return axios.get('/products').then(
          response => dispatch(fetchItemsSuccess(response.data))
        );
    };
};

export const createItem = itemData => {
    return dispatch => {
        return axios.post('/items', itemData).then(
            () => dispatch(createItemsSuccess())
        );
    };
};