import {push} from 'connected-react-router';
import axios from '../../axiosapi';
export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILURE = 'REGISTER_USER_FAILURE';

const registerUserSuccess = () => ({type: REGISTER_USER_SUCCESS});

const registerUserFailure = error => ({type: REGISTER_USER_FAILURE, error});

export const registerUser = userData => {
    return dispatch => {
        return axios.post('/users', userData).then(
            () =>  {
                dispatch(registerUserSuccess());
                dispatch(push('/'));
            },
            error => {
                if (error.response) {
                    dispatch(registerUserFailure(error.response.data))
                } else {
                    dispatch(registerUserFailure({global: 'No connection'}))
                }
            }
        )
    }
};