const mongoose = require('mongoose');
const config = require('./config');

const Item = require('./models/Item');
const Category = require('./models/Category');


const run = async () => {
    await mongoose.connect(config.dbUrl,config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const [allItems,forHome, cars, electronicsGadgets, theProperty, others] = await Category.create(
        {title:'All items'},
        {title:'For home'},
        {title:'Cars'},
        {title:'electronics gadgets'},
        {title:'the property'},
        {title:'others'}
    );

    const items = await Item.create(
        {
            title: 'antique vase',
            price: 5000,
            description: 'Antique Matched Pair of Old Paris Porcelain Hand Painted Bolted Vases/Urns with Figural Handles',
            category: forHome._id,
            image: ""
        },
        {
            title: 'Pre-Owned 1941 Ford Convertible',
            price: 70000000,
            description: 'This unrestored low production car has been off the road for 50 or more years and is an ideal restoration or street rod candidate. Priced to sell at $7,888 or make off. Call Harold at 315-439-6348',
            category: cars._id,
            image: ""
        },
        {
            title: 'Xbox Adaptive Controller',
            price: 13000,
            description: 'Xbox Adaptive controller is easily one of the most unique gadgets that we’ve seen in 2018. especially, since you can completely customize it based on someone’s needs.',
            category: electronicsGadgets._id,
            image: ""
        }
    );


    return connection.close();
};

run().catch(error => {
    console.error('Something wrong happened', error);
});